/* tslint:disable:max-line-length */
import {User} from '../models/user.model';
import {Room} from '../models/room.model';
import {Message} from '../models/message.model';
import {MessagesService} from '../services/messages.service';
import {RoomsService} from '../services/rooms.service';
import {UsersService} from '../services/users.service';
import * as moment from 'moment';

// the person using the app us Juliet
const me: User = new User('Juliet', '', 'assets/images/avatars/female-avatar-1.png', '1');
const ladycap: User = new User('Lady Capulet', '', 'assets/images/avatars/female-avatar-2.png', '2');
const echo: User = new User('Echo Bot', '', 'assets/images/avatars/male-avatar-1.png', '3');
const rev: User = new User('Reverse Bot', '', 'assets/images/avatars/female-avatar-4.png', '4');
const wait: User = new User('Waiting Bot', '', 'assets/images/avatars/male-avatar-2.png', '5');

const tLadycap: Room = new Room('tLadycap', ladycap.name, ladycap.avatar);
const tEcho: Room = new Room('tEcho', echo.name, echo.avatar);
const tRev: Room = new Room('tRev', rev.name, rev.avatar);
const tWait: Room = new Room('tWait', wait.name, wait.avatar);

const initialMessages: Array<Message> = [
  new Message({
    author: me,
    createdAt: moment().subtract(45, 'minutes').toDate(),
    text: 'Yet let me weep for such a feeling loss.',
    room: tLadycap
  }),
  new Message({
    author: ladycap,
    createdAt: moment().subtract(20, 'minutes').toDate(),
    text: 'So shall you feel the loss, but not the friend which you weep for.',
    room: tLadycap
  }),
  new Message({
    author: echo,
    createdAt: moment().subtract(1, 'minutes').toDate(),
    text: `I\'ll echo whatever you send me`,
    room: tEcho
  }),
  new Message({
    author: rev,
    createdAt: moment().subtract(3, 'minutes').toDate(),
    text: `I\'ll reverse whatever you send me`,
    room: tRev
  }),
  new Message({
    author: wait,
    createdAt: moment().subtract(4, 'minutes').toDate(),
    text: `I\'ll wait however many seconds you send to me before responding. Try sending '3'`,
    room: tWait
  }),
];

export class ChatExampleData {
  static init(messagesService: MessagesService,
              roomsService: RoomsService,
              UsersService: UsersService): void {

    // TODO make `messages` hot
    messagesService.messages.subscribe(() => ({}));

    // set "Juliet" as the current user
    UsersService.setCurrentUser(me);

    // create the initial messages
    initialMessages.map((message: Message) => messagesService.addMessage(message));

    roomsService.setCurrentRoom(tEcho);

    this.setupBots(messagesService);
  }

  static setupBots(messagesService: MessagesService): void {

    // echo bot
    messagesService.messagesForRoomUser(tEcho, echo)
      .forEach((message: Message): void => {
          messagesService.addMessage(
            new Message({
              author: echo,
              text: message.content,
              room: tEcho
            })
          );
        },
        null);


    // reverse bot
    messagesService.messagesForRoomUser(tRev, rev)
      .forEach((message: Message): void => {
          messagesService.addMessage(
            new Message({
              author: rev,
              text: message.content.split('').reverse().join(''),
              room: tRev
            })
          );
        },
        null);

    // waiting bot
    messagesService.messagesForRoomUser(tWait, wait)
      .forEach((message: Message): void => {

          let waitTime: number = parseInt(message.content, 10);
          let reply: string;

          if (isNaN(waitTime)) {
            waitTime = 0;
            reply = `I didn\'t understand ${message.content}. Try sending me a number`;
          } else {
            reply = `I waited ${waitTime} seconds to send you this.`;
          }

          setTimeout(
            () => {
              messagesService.addMessage(
                new Message({
                  author: wait,
                  text: reply,
                  room: tWait
                })
              );
            },
            waitTime * 1000);
        },
        null);


  }
}
