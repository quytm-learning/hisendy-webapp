import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {RoomsService} from './rooms.service';
import {UsersService} from './users.service';
import {Room} from '../models/room.model';
import {MessagesService} from './messages.service';
import {User} from '../models/user.model';
import {Message} from '../models/message.model';
import * as moment from 'moment';
import {SocketService} from './socket.service';
import {LocalStorageService} from './localStorage.service';
import * as _ from 'lodash';

@Injectable({
  providedIn: 'root'
})
export class HttpService {

  private BASE_URL = 'http://localhost:3000/api';

  constructor(private http: HttpClient,
              private roomsService: RoomsService,
              private messageService: MessagesService,
              private userService: UsersService,
              private localStorageService: LocalStorageService,
              private socketService: SocketService) {

  }

  public getUserInfo(userId) {
    const url = `${this.BASE_URL}/users/${userId}`;
    return this.http.get(url);
  }

  public fetchAllDataForChatting() {
    const currentUser = this.localStorageService.loadUser();

    // this.fetchAllMessage(currentUser);
    this.findAllRooms(currentUser)
      .then((isOk: boolean) => {
        if (isOk) {
          this.fetchAllMessage(currentUser);
        }
      });
  }

  public findAllRooms(currentUser): Promise<boolean> {
    const ROOMS = `${this.BASE_URL}/users/${currentUser._id}/rooms`;
    return this.http.get(ROOMS)
      .toPromise()
      .then((rooms: Array<Room>) => {
        rooms.forEach(room => {
          const lastMessage = room.lastMessage;
          if (!_.isEmpty(lastMessage)) {
            console.log(lastMessage);
            lastMessage.content = '';
            lastMessage.createdAt = new Date(0);

            // const tmpLastMessage = new Message(lastMessage);
            // tmpLastMessage.room = null;
            // const tmpRoom = new Room(room._id, room.name, room.avatar);
            // tmpRoom.lastMessage = tmpLastMessage;
            // lastMessage.room = tmpRoom;

            lastMessage.room = room;

            console.log(lastMessage);

            this.messageService.addMessage(lastMessage);
            this.roomsService.setCurrentRoom(room);
            this.socketService.joinRoom(room);
          }
        });
        return Promise.resolve(true);
      });
  }

  public fetchAllMessage(currentUser): void {
    const url = `${this.BASE_URL}/users/${currentUser._id}/messages`;
    this.http.get(url)
      .subscribe((messages: Array<Message>) => {
        // for (let i = 0; i < messages.length; i++) {
        //   // console.log(message);
        //   this.messageService.addMessage(messages[i]);
        // }
        messages.forEach((message: Message) => {
          // console.log(message);
          this.messageService.addMessage(message);
        });
      });
  }
}
