import { MessagesService } from './messages.service';

import { Message } from '../models/message.model';
import { Room } from '../models/room.model';
import { User } from '../models/user.model';

describe('MessagesService', () => {
  it('should test', () => {

    const user: User = new User('Nate', '', '', '');
    const room: Room = new Room('t1', 'Nate', '');
    const m1: Message = new Message({
      author: user,
      text: 'Hi!',
      room: room
    });

    const m2: Message = new Message({
      author: user,
      text: 'Bye!',
      room: room
    });

    const messagesService: MessagesService = new MessagesService();

    // listen to each message indivdually as it comes in
    messagesService.newMessages
      .subscribe( (message: Message) => {
        console.log('=> newMessages: ' + message.content);
      });

    // listen to the stream of most current messages
    messagesService.messages
      .subscribe( (messages: Message[]) => {
        console.log('=> messages: ' + messages.length);
      });

    messagesService.addMessage(m1);
    messagesService.addMessage(m2);

    // => messages: 1
    // => newMessages: Hi!
    // => messages: 2
    // => newMessages: Bye!
  });


});
