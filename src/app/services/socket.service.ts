import {Injectable, OnInit} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {MessageSocket} from '../models/socket/messageSocket.model';
import {Message, Message as ChatMessage} from '../models/message.model';
import {Event} from '../models/socket/event.model';

import * as socketIo from 'socket.io-client';
import {RoomServer, User} from '../models/user.model';
import {MessagesService} from './messages.service';
import {UsersService} from './users.service';
import {Room} from '../models/room.model';
import {RoomsService} from './rooms.service';

const SERVER_URL = 'http://localhost:3000';
const ROOM_SOCKET_URL = `${SERVER_URL}/rooms`;
const CHAT_SOCKET_URL = `${SERVER_URL}/chats`;

@Injectable({
  providedIn: 'root'
})
export class SocketService implements OnInit {

  private messageSocket;
  private roomSocket;
  private currentRoomId;
  private currentUser: User;
  private currentRoom: Room;

  constructor(private messagesService: MessagesService,
              private userService: UsersService,
              private roomsService: RoomsService) {
  }

  ngOnInit(): void {
    this.userService.currentUser.subscribe((user: User) => this.currentUser = user);
    this.roomsService.currentRoom.subscribe((room: Room) => this.currentRoom = room);
  }

  public initSocket(): void {
    this.messageSocket = socketIo(CHAT_SOCKET_URL);
    this.roomSocket = socketIo(ROOM_SOCKET_URL);
  }

  public send(message: MessageSocket): void {
    this.messageSocket.emit('message', message);
  }

  public onMessage(): Observable<MessageSocket> {
    return new Observable<MessageSocket>(observer => {
      this.messageSocket.on('message', (data: MessageSocket) => observer.next(data));
    });
  }

  public onEvent(event: Event): Observable<any> {
    return new Observable<Event>(observer => {
      this.messageSocket.on(event, () => observer.next());
    });
  }

  public joinRoom(room: any): void {
    // console.log(`Join into room: ${JSON.stringify(room)}`);
    this.messageSocket.emit('joinRoom', {_id: room._id});
  }

  public onEvent_temp() {
    this.roomSocket.on('updateRoom', (data) => {
      console.log('New room:');
      console.log(data);
      const roomId = data.room._id;
      this.currentRoomId = roomId;
      this.messageSocket.emit('joinRoom', {roomId: roomId});
    });
    this.messageSocket.on('addMessage', (message: Message) => {
      console.log('New message: ');
      console.log(message);
      const m: ChatMessage = new ChatMessage();
      m.content = message.content;
      // m.author = this.currentUser;
      // m.author = new User('temp_name', '', 'assets/images/avatars/male-avatar-2.png', message.author);
      // m.room = new Room(message.room, 'temp_name_2', 'assets/images/avatars/male-avatar-2.png');
      m.author = message.author;
      m.room = message.room;
      // m.room = this.currentRoom;
      m.isRead = false;
      console.log(m);
      this.messagesService.addMessage(m);
    });
  }

  public createNewRoom() {
    this.roomSocket.emit('createRoom', {leftUser: '123', rightUser: '112'});
  }

  // public sendNewMessage(roomId, message) {
  //   this.messageSocket.emit('newMessage', {roomId: roomId, message: message});
  // }

  public sendNewMessage(message: Message) {
    const payload = {
      author: message.author._id,
      content: message.content,
      room: message.room._id
    };
    this.messageSocket.emit('newMessage', payload);
  }
}
