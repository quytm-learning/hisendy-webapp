import {Injectable} from '@angular/core';
import {Subject, BehaviorSubject, Observable} from 'rxjs/Rx';
import {Room} from '../models/room.model';
import {Message} from '../models/message.model';
import {MessagesService} from './messages.service';
import * as _ from 'lodash';

@Injectable()
export class RoomsService {

  // `rooms` is a observable that contains the most up to date list of rooms
  rooms: Observable<{ [key: string]: Room }>;

  // `orderedRooms` contains a newest-first chronological list of rooms
  orderedRooms: Observable<Room[]>;

  // `currentRoom` contains the currently selected room
  currentRoom: Subject<Room> =
    new BehaviorSubject<Room>(new Room());

  // `currentRoomMessages` contains the set of messages for the currently
  // selected room
  currentRoomMessages: Observable<Message[]>;

  constructor(public messagesService: MessagesService) {

    this.rooms = messagesService.messages
      .map((messages: Message[]) => {
        const rooms: { [key: string]: Room } = {};
        // Store the message's room in our accumulator `rooms`
        messages.map((message: Message) => {
          rooms[message.room._id] = rooms[message.room._id] || message.room;

          // Cache the most recent message for each room
          const messagesRoom: Room = rooms[message.room._id];
          // console.log(`trigger: message = ${message.content}`);
          // console.log(`Conditional: '${messagesRoom.lastMessage.content}' ${messagesRoom.lastMessage.createdAt}`);
          if (!messagesRoom.lastMessage || messagesRoom.lastMessage.createdAt <= message.createdAt) {
            // console.log(`Set lastMessage ${message.content}`);
            messagesRoom.lastMessage = message;
          } else {
            // console.log(`Cannot set lastMessage ${message.content}`);
          }
        });
        return rooms;
      });

    this.orderedRooms = this.rooms
      .map((roomGroups: { [key: string]: Room }) => {
        const rooms: Room[] = _.values(roomGroups);
        return _.sortBy(rooms, (t: Room) => t.lastMessage.createdAt).reverse();
      });

    this.currentRoomMessages = this.currentRoom
      .combineLatest(messagesService.messages,
        (currentRoom: Room, messages: Message[]) => {
          if (currentRoom && messages.length > 0) {
            return _.chain(messages)
              .filter((message: Message) =>
                (message.room._id === currentRoom._id))
              .map((message: Message) => {
                message.isRead = true;
                return message;
              })
              .value();
          } else {
            return [];
          }
        });

    this.currentRoom.subscribe(this.messagesService.markRoomAsRead);
  }

  setCurrentRoom(newRoom: Room): void {
    this.currentRoom.next(newRoom);
  }

}

export const roomsServiceInjectables: Array<any> = [
  RoomsService
];
