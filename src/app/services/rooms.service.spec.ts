import { Message } from '../models/message.model';
import { Room } from '../models/room.model';
import { User } from '../models/user.model';

import { RoomsService } from './rooms.service';
import { MessagesService } from './messages.service';
import * as _ from 'lodash';

describe('RoomsService', () => {
  it('should collect the Rooms from Messages', () => {

    const nate: User = new User('Nate Murray', '', '', '');
    const felipe: User = new User('Felipe Coury', '', '', '');

    const t1: Room = new Room('t1', 'Room 1', '');
    const t2: Room = new Room('t2', 'Room 2', '');

    const m1: Message = new Message({
      author: nate,
      text: 'Hi!',
      room: t1
    });

    const m2: Message = new Message({
      author: felipe,
      text: 'Where did you get that hat?',
      room: t1
    });

    const m3: Message = new Message({
      author: nate,
      text: 'Did you bring the briefcase?',
      room: t2
    });

    const messagesService: MessagesService = new MessagesService();
    const roomsService: RoomsService = new RoomsService(messagesService);

    roomsService.rooms
      .subscribe( (roomIdx: { [key: string]: Room }) => {
        const rooms: Room[] = _.values(roomIdx);
        const roomNames: string = _.map(rooms, (t: Room) => t.name)
                                   .join(', ');
        console.log(`=> rooms (${rooms.length}): ${roomNames} `);
      });

    messagesService.addMessage(m1);
    messagesService.addMessage(m2);
    messagesService.addMessage(m3);

    // => rooms (1): Room 1
    // => rooms (1): Room 1
    // => rooms (2): Room 1, Room 2

  });
});
