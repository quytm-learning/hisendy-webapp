import {Injectable} from '@angular/core';
import {User} from '../models/user.model';
import {UsersService} from './users.service';

@Injectable()
export class LocalStorageService {

  constructor(private usersService: UsersService) {
  }

  public loadUser(): User {
    const localUser = JSON.parse(localStorage.getItem('user'));
    let currentUser: User = new User('Guest', '', 'assets/images/avatars/female-avatar-1.png', '11');
    if (localUser) {
      currentUser = new User(localUser.name, localUser.email, localUser.avatar, localUser._id);
      this.usersService.setCurrentUser(currentUser);
    }
    return currentUser;
  }

}
