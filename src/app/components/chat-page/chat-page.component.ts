import {Component, Inject, OnInit} from '@angular/core';
import {ChatExampleData} from '../../data/chat-example-data';

import {UsersService} from '../../services/users.service';
import {RoomsService} from '../../services/rooms.service';
import {MessagesService} from '../../services/messages.service';

import {Action, Event} from '../../models/socket/event.model';
import {MessageSocket, User} from '../../models/socket/messageSocket.model';
import {SocketService} from '../../services/socket.service';
import {HttpService} from '../../services/http.service';

@Component({
  selector: 'chat-page',
  templateUrl: './chat-page.component.html',
  styleUrls: ['./chat-page.component.css']
})
export class ChatPageComponent implements OnInit {
  action = Action;

  user: User;
  messages: MessageSocket[] = [];
  messageContent: string;
  ioConnection: any;

  constructor(public messagesService: MessagesService,
              public roomsService: RoomsService,
              public usersService: UsersService,
              private socketService: SocketService,
              private httpService: HttpService) {
    // ChatExampleData.init(messagesService, roomsService, usersService);
    this.httpService.fetchAllDataForChatting();
  }

  ngOnInit(): void {
    this.initIoConnection();
  }

  private initIoConnection(): void {
    this.socketService.initSocket();

    this.ioConnection = this.socketService.onMessage()
      .subscribe((message: MessageSocket) => {
        this.messages.push(message);
      });

    this.socketService.onEvent(Event.CONNECT)
      .subscribe(() => {
        console.log('connected');
      });

    this.socketService.onEvent(Event.DISCONNECT)
      .subscribe(() => {
        console.log('disconnected');
      });

    this.socketService.onEvent_temp();
  }

  public sendMessage(message: string): void {
    if (!message) {
      return;
    }

    this.socketService.send({
      from: this.user,
      content: message
    });
    this.messageContent = null;
  }

  public sendNotification(params: any, action: Action): void {
    let message: MessageSocket;

    if (action === Action.JOINED) {
      message = {
        from: this.user,
        action: action
      };
    } else if (action === Action.RENAME) {
      message = {
        action: action,
        content: {
          username: this.user.name,
          previousUsername: params.previousUsername
        }
      };
    }

    this.socketService.send(message);
  }

}
