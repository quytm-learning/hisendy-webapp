import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter
} from '@angular/core';
import { Observable } from 'rxjs';
import { RoomsService } from '../../services/rooms.service';
import { Room } from '../../models/room.model';

@Component({
  selector: 'chat-room',
  templateUrl: './chat-room.component.html',
  styleUrls: ['./chat-room.component.css']
})
export class ChatRoomComponent implements OnInit {
  @Input() room: Room;
  selected = false;

  constructor(public roomsService: RoomsService) {
  }

  ngOnInit(): void {
    this.roomsService.currentRoom
      .subscribe( (currentRoom: Room) => {
        this.selected = currentRoom &&
          this.room &&
          (currentRoom._id === this.room._id);
      });
  }

  clicked(event: any): void {
    this.roomsService.setCurrentRoom(this.room);
    event.preventDefault();
  }
}
