import {
  Component,
  Inject,
  ElementRef,
  OnInit,
  ChangeDetectionStrategy
} from '@angular/core';
import {Observable} from 'rxjs';

import {User} from '../../models/user.model';
import {UsersService} from '../../services/users.service';
import {Room} from '../../models/room.model';
import {RoomsService} from '../../services/rooms.service';
import {Message} from '../../models/message.model';
import {MessagesService} from '../../services/messages.service';
import {SocketService} from '../../services/socket.service';
import * as _ from 'lodash';

@Component({
  selector: 'chat-messages',
  templateUrl: './chat-messages.component.html',
  styleUrls: ['./chat-messages.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ChatMessagesComponent implements OnInit {
  messages: Observable<any>;
  currentRoom: Room;
  draftMessage: Message;
  currentUser: User;

  constructor(public messagesService: MessagesService,
              public roomsService: RoomsService,
              public UsersService: UsersService,
              public socketService: SocketService,
              public el: ElementRef) {
  }

  ngOnInit(): void {
    this.messages = this.roomsService.currentRoomMessages;

    this.draftMessage = new Message();

    this.roomsService.currentRoom.subscribe(
      (room: Room) => {
        this.currentRoom = room;
      });

    this.UsersService.currentUser
      .subscribe(
        (user: User) => {
          this.currentUser = user;
        });

    this.messages
      .subscribe(
        (messages: Array<Message>) => {
          setTimeout(() => {
            this.scrollToBottom();
          });
        });
  }

  onEnter(event: any): void {
    this.sendMessage();
    event.preventDefault();
  }

  sendMessage(): void {
    if (_.isEmpty(this.draftMessage.content)) return;

    const m: Message = this.draftMessage;
    m.author = this.currentUser;
    m.room = this.currentRoom;
    m.isRead = true;

    this.messagesService.addMessage(m);
    this.socketService.sendNewMessage(m);

    this.draftMessage = new Message();
  }

  scrollToBottom(): void {
    const scrollPane: any = this.el.nativeElement.querySelector('.panel-body');
    scrollPane.scrollTop = scrollPane.scrollHeight;
  }
}
