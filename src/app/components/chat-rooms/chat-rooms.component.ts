import {
  Component,
  OnInit,
  Inject
} from '@angular/core';
import { Observable } from 'rxjs';
import { Room } from '../../models/room.model';
import { RoomsService } from '../../services/rooms.service';

@Component({
  selector: 'chat-rooms',
  templateUrl: './chat-rooms.component.html',
  styleUrls: ['./chat-rooms.component.css']
})
export class ChatRoomsComponent {
  rooms: Observable<any>;

  constructor(public roomsService: RoomsService) {
    this.rooms = roomsService.orderedRooms;
  }
}
