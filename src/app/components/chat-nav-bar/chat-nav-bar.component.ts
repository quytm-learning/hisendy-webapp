import {
  Component,
  Inject,
  OnInit
} from '@angular/core';
import * as _ from 'lodash';

import { RoomsService } from '../../services/rooms.service';
import { MessagesService } from '../../services/messages.service';

import { Room } from '../../models/room.model';
import { Message } from '../../models/message.model';
import {UsersService} from '../../services/users.service';
import {User} from '../../models/user.model';
import {HttpService} from '../../services/http.service';

@Component({
  selector: 'chat-nav-bar',
  templateUrl: './chat-nav-bar.component.html',
  styleUrls: ['./chat-nav-bar.component.css']
})
export class ChatNavBarComponent implements OnInit {
  unreadMessagesCount: number;
  userFullName: string;

  constructor(public messagesService: MessagesService,
              public roomsService: RoomsService,
              public userService: UsersService,
              private httpService: HttpService) {
  }

  ngOnInit(): void {
    const user = localStorage.getItem('user');

    this.userFullName = _.isEmpty(user) ? 'Guest' : JSON.parse(user).name;

    this.messagesService.messages
      .combineLatest(
        this.roomsService.currentRoom,
        (messages: Message[], currentRoom: Room) =>
          [currentRoom, messages] )

      .subscribe(([currentRoom, messages]: [Room, Message[]]) => {
        this.unreadMessagesCount =
          _.reduce(
            messages,
            (sum: number, m: Message) => {
              const messageIsInCurrentRoom: boolean = m.room &&
                currentRoom &&
                (currentRoom._id === m.room._id);
              // note: in a "real" app you should also exclude
              // messages that were authored by the current user b/c they've
              // already been "read"
              if (m && !m.isRead && !messageIsInCurrentRoom) {
                sum = sum + 1;
              }
              return sum;
            },
            0);
      });
  }

  sendUserInfo(): void {
    const roomId = (<HTMLInputElement>document.getElementById('input-user-id')).value;

    console.log(roomId, name);
    // set current user
    // const currentUser: User = new User(name, '', 'assets/images/avatars/female-avatar-1.png', roomId);
    this.httpService.getUserInfo(roomId)
      .subscribe((user: User) => {
        this.userService.setCurrentUser(user);
        localStorage.setItem('user', JSON.stringify(user));
        window.location.reload();
      });
  }

  closeDialog(): void {
    (<any>document.getElementById('user-dialog')).close();
  }
  showDialog(): void {
    (<any>document.getElementById('user-dialog')).showModal();
  }

}
