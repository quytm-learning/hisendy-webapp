import {Action} from './event.model';

export interface User {
  id?: number;
  name?: string;
  avatar?: string;
}

export interface MessageSocket {
  from?: User;
  content?: any;
  action?: Action;
}

// export class ChatMessage implements Message {
//   constructor(from: User, content: string) {
//     super(from, content);
//   }
// }
