import {Message} from './message.model';
import {uuid} from '../util/uuid';
import {User} from './user.model';

/**
 * Room represents a group of Users exchanging Messages
 */
export class Room {
  _id: string;
  leftUser: User;
  rightUser: User;
  lastMessage: Message;
  name: string;
  avatar: string;

  constructor(id?: string,
              name?: string,
              avatar?: string) {
    this._id = id || uuid();
    this.name = name;
    this.avatar = avatar;
  }

  public setLeftUser(leftUser: User): void {
    this.leftUser = leftUser;
  }

  public setRightUser(rightUser: User): void {
    this.rightUser = rightUser;
  }

}
