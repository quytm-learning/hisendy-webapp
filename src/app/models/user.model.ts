import {uuid} from '../util/uuid';

/**
 * A User represents an agent that sends messages
 */
export class User {
  // _id: string;
  sex: string;
  bio: string;

  constructor(public name: string,
              public email: string,
              public avatar: string,
              public _id: string) {
    // this._id = id || uuid();
  }
}

export class UserDTO {
  _id: string;
  name: string;
  avatar: string;
  room: RoomServer;

  constructor(_id?: string,
              name?: string,
              avatar?: string,
              room?: RoomServer) {
    this._id = _id || uuid();
    this.name = name;
    this.avatar = avatar;
    this.room = room;
  }
}

export class RoomServer {

  constructor(private _id: string,
              private leftUser: string,
              private rightUser: string,
              private createdAt: Date) {
  }
}
