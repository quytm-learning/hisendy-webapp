import { User } from './user.model';
import { Room } from './room.model';
import { uuid } from '../util/uuid';

/**
 * Message represents one message being sent in a Room
 */
 export class Message {
   id: string;
   createdAt: Date;
   isRead: boolean;
   author: User;
   content: string;
   room: Room;

   constructor(obj?: any) {
     this.id              = obj && obj.id              || uuid();
     this.isRead          = obj && obj.isRead          || false;
     this.createdAt       = obj && obj.createdAt       || new Date();
     this.author          = obj && obj.author          || null;
     this.content         = obj && obj.content         || null;
     this.room            = obj && obj.room            || null;
   }
 }
