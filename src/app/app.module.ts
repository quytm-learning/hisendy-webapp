import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';

import {UsersService} from './services/users.service';
import {RoomsService} from './services/rooms.service';
import {MessagesService} from './services/messages.service';

import {AppComponent} from './app.component';
import {ChatMessageComponent} from './components/chat-message/chat-message.component';
import {ChatRoomComponent} from './components/chat-room/chat-room.component';
import {ChatNavBarComponent} from './components/chat-nav-bar/chat-nav-bar.component';
import {ChatRoomsComponent} from './components/chat-rooms/chat-rooms.component';
import {ChatMessagesComponent} from './components/chat-messages/chat-messages.component';
import {ChatPageComponent} from './components/chat-page/chat-page.component';
import {FromNowPipe} from './pipes/from-now.pipe';
import {SocketService} from './services/socket.service';
import {HttpService} from './services/http.service';
import {LocalStorageService} from './services/localStorage.service';

@NgModule({
  declarations: [
    AppComponent,
    ChatMessageComponent,
    ChatRoomComponent,
    ChatNavBarComponent,
    ChatRoomsComponent,
    ChatMessagesComponent,
    ChatPageComponent,
    FromNowPipe
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [
    MessagesService,
    RoomsService,
    UsersService,
    SocketService,
    HttpService,
    LocalStorageService
  ],

  bootstrap: [AppComponent]
})
export class AppModule {
}
